module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      keyframes: {
        wiggle: {
          '0%': { transform: 'translateX(0)' },
          '15%': { transform: 'translateX(0.375rem)' },
          '30%': { transform: 'translateX(-0.375rem)' },
          '45%': { transform: 'translateX(0.375rem)' },
          '60%': { transform: 'translateX(-0.375rem)' },
          '75%': { transform: 'translateX(0.375rem)' },
          '90%': { transform: 'translateX(-0.375rem)' },
          '100%': { transform: 'translateX(0)' }
        }
      },
      animation: {
        wiggle: 'wiggle .6s forwards ease-in-out'
      }
    },
    fontFamily: {
      'oswald': ['Oswald'],
      'opensans': ['Open Sans']
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
