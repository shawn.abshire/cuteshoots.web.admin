import { Box } from '@/core/components';
import Lull from 'lull-ui';
import { createApp } from 'vue';
import '../node_modules/lull-ui/dist/style.css';
import App from './App.vue';
import './assets/css/base.scss';
import './assets/css/compiled-tailwind.min.css';
import './assets/css/index.css';
import router from './router/router';
import { key, store } from './store';

const app = createApp(App)
    .use(store, key)
    .use(router)
    .use(Lull);

app.component('box', Box);

// console.log('app components', app._context.components);

app.mount('#app');
