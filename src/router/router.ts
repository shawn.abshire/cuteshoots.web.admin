import { AuthService } from '@/core/services/identity/AuthService';
import { IAuthService } from '@/core/services/identity/IAuthService';
import { ManageAction } from '@/core/types/ManageAction';
import store from '@/store';
import { AuthAction } from '@/store/Auth/AuthModule';
import Client from '@/views/clients/Client.vue';
import DashboardView from '@/views/DashboardView.vue';
import Page from '@/views/Page.vue';
import Role from '@/views/roles/Role.vue';
import User from '@/views/users/User.vue';
import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  { path: '/', redirect: '/dashboard' },
  { path: '/noop', name: 'noop', component: () => import(/* webpackChunkName: "noop" */ '../views/Noop.vue') },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: DashboardView,
    children: [
      {
        path: '',
        component: () => import(/* webpackChunkName: "dashboard" */ '../views/dashboard/Dashboard.vue'),
      },
      {
        path: '/clients',
        name: 'clients',
        component: Page,
        children: [
          { path: '', component: () => import(/* webpackChunkName: "clients" */ '../views/clients/Clients.vue'), },
          { path: 'new', component: Client, props: { action: ManageAction.Create } },
          { path: ':id', component: Client, props: { action: ManageAction.Edit } }
        ]
      },
      {
        path: '/users',
        name: 'users',
        component: Page,
        children: [
          { path: '', component: () => import(/* webpackChunkName: "clients" */ '../views/users/Users.vue') },
          { path: 'new', component: User, props: { action: ManageAction.Create } },
          { path: ':id', component: User, props: { action: ManageAction.Edit } }
        ]
      },
      {
        path: '/roles',
        name: 'roles',
        component: Page,
        children: [
          { path: '', component: () => import(/* webpackChunkName: "clients" */ '../views/roles/Roles.vue') },
          { path: 'new', component: Role, props: { action: ManageAction.Create } },
          { path: ':id', component: Role, props: { action: ManageAction.Edit } }
        ]
      }

    ]
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

const hasPermission = async (): Promise<boolean> => {
  const authService: IAuthService = new AuthService();
  const hasPermission = await authService.hasRoleClaim(import.meta.env.VITE_APP_REQUIRED_ROLE as string);

  return hasPermission;
}

router.beforeEach(async (to, _from, next) => {
  const user = await store.getters.userAuthenticated;
  if (!user) {
    await store.dispatch(AuthAction.GetUser);
  }

  const viewable = await hasPermission();
  if (!viewable && to.name !== 'noop') {
    return next('/noop');
  }

  next();
});

export default router;
