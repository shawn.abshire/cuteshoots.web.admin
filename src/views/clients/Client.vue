<template>
  <box :loading="loading">
    <template v-slot:title>{{ capitalizeFirstLetter(action) }} Client</template>

    <div class="px-3 w-full">
      <div class="flex flex-row">
        <lull-input
          class="w-3/4 pr-5"
          label="Client Name:"
          :class="[hasError('name') && 'shake']"
          :message="getValidationMessage('name')"
          :status="getValidationStatus('name')"
          v-model="client.name"
        />

        <lull-select
          class="w-1/4"
          label="Grant Type:"
          v-model="client.grantType.grantType"
          :class="[hasError('name') && 'shake']"
          :message="getValidationMessage('grantType')"
          :status="getValidationStatus('grantType')"
          :options="grantOptions"
          filterable
        />
      </div>

      <div class="pt-5" v-if="action === ManageAction.Edit">
        <lull-input
          label="Client Guid:"
          disabled
          readonly
          v-model="client.clientGuid"
        />
      </div>

      <div class="pt-5 flex flex-row">
        <lull-input-tag
          class="w-11/12"
          label="Scopes:"
          v-model="scopes"
          placeholder="Add Scope"
          :allowDuplicated="false"
          @addTag="addScope($event)"
          @removeTag="removeScope($event)"
        />

        <div class="w-1/12 flex flex-col items-center justify-center">
          <label>PKCE Enabled:</label>
          <lull-switch
            name="PKCE Enabled"
            v-model="client.pkceEnabled"
            :disabled="pkceEnabled()"
          />
        </div>
      </div>

      <div class="pt-5" v-if="action === ManageAction.Edit">
        <cororigins :clientId="$route.params.id" />
      </div>

      <div class="pt-5" v-if="action === ManageAction.Edit">
        <redirecturls :clientId="$route.params.id" />
      </div>

      <div class="pt-5" v-if="action === ManageAction.Edit">
        <secrets :clientId="$route.params.id" />
      </div>
    </div>

    <template v-slot:footer>
      <div class="flex flex-row justify-end px-3 pt-3 pr-6 pb-6">
        <lull-button
          type="danger"
          class="w-28"
          @click="$router.push('/clients')"
        >
          Cancel
        </lull-button>
        <lull-button type="info" class="ml-3 w-28" @click="save()">
          Save
        </lull-button>
      </div>
    </template>
  </box>
</template>

<script lang="ts">
import {
  computed,
  defineComponent,
  onBeforeMount,
  onBeforeUnmount,
  reactive,
  ref,
  watch,
} from 'vue';
import {
  capitalizeFirstLetter,
  enumToSelectOptions,
} from '@/core/helpers/util';
import { GrantType } from '@/core/types/GrantType';
import { SelectOption } from '@/core/types/SelectOption';
import { ManageAction } from '@/core/types/ManageAction';
import { useStore } from '@/store';
import { useRouter, useRoute } from 'vue-router';
import { ClientScope } from '@/core/models/client/ClientScope';
import Secrets from './Secrets.vue';
import RedirectUrls from './RedirectUrls.vue';
import { ClientAction, ClientModule } from '@/store/Clients/ClientModule';
import { Client } from '@/core/models/client/Client';
import { required } from '@vuelidate/validators';
import useValidation from '@/core/composables/useValidations';
import { useLullToast } from 'lull-ui';
import CoreOrigins from './CorsOrigins.vue';

export default defineComponent({
  name: 'client',
  components: {
    secrets: Secrets,
    redirecturls: RedirectUrls,
    cororigins: CoreOrigins,
  },
  props: {
    action: { type: String, required: true, default: ManageAction.Create },
  },
  setup(props) {
    const store = useStore();
    const router = useRouter();
    const route = useRoute();
    const grantEnum = enumToSelectOptions(GrantType);
    const grantOptions = grantEnum.map((x: SelectOption) => x.label);
    const loading = ref<boolean>(false);
    const key = ref<string>('abc');
    const toast = useLullToast();

    const client: Partial<Client> = reactive({
      pkceEnabled: false,
      grantType: {
        grantType: GrantType.Implicit,
      },
      scopes: [],
    });

    const rules = {
      name: { required },
      grantType: {
        grantType: { required },
      },
    };

    const { validation, getValidationMessage, getValidationStatus, hasError } =
      useValidation(rules, client);

    const scopes = computed(() =>
      client.scopes?.map((x: ClientScope) => x.scope)
    );

    async function save(): Promise<void> {
      loading.value = true;
      validation.value.$touch();
      if (validation.value.$invalid) {
        loading.value = false;
        return;
      }

      if (props.action === ManageAction.Create) {
        const id = await store.dispatch(
          `client/${ClientAction.AddClient}`,
          client
        );
        if (id) {
          client.clientGuid = id;
          toast.success({
            title: 'Success',
            message: 'Successfully created new client.',
          });
          router.push(`/clients/${id}`);
        }
      }

      if (props.action === ManageAction.Edit) {
        const results = await store.dispatch(
          `client/${ClientAction.UpdateClient}`,
          client
        );

        if (results) {
          toast.success({
            title: 'Success',
            message: 'Successfully saved client details.',
          });
          router.push(`/clients`);
        }
      }

      loading.value = false;
    }

    async function addScope(value: string): Promise<void> {
      const scope: ClientScope = { scope: value };
      client.scopes?.push(scope);
    }

    function removeScope({ idx, tag }: { idx: number; tag: string }): void {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const scope = (client.scopes as any)[idx];
      if (scope.scope === tag) {
        client.scopes?.splice(idx, 1);
      }
    }

    async function setup(): Promise<void> {
      loading.value = true;
      if (props.action === ManageAction.Edit) {
        const results = await store.dispatch(
          `client/${ClientAction.GetClient}`,
          route.params.id
        );
        Object.assign(client, results);
      }
      loading.value = false;
    }

    function pkceEnabled() {
      const results =
        client.grantType?.grantType !== GrantType.Code &&
        client.grantType?.grantType !== GrantType.CodeAndClientCredentials;

      return results;
    }

    watch(
      () => client.grantType?.grantType,
      (grantType) => {
        if (
          grantType !== GrantType.Code &&
          grantType !== GrantType.CodeAndClientCredentials
        ) {
          client.pkceEnabled = false;
        }
      }
    );

    onBeforeMount(async () => {
      await store.registerModule('client', ClientModule);
      await setup();
    });

    onBeforeUnmount(async () => {
      await store.unregisterModule('client');
    });

    return {
      capitalizeFirstLetter,
      grantOptions,
      key,
      loading,
      ManageAction,
      client,
      save,
      scopes,
      addScope,
      removeScope,
      getValidationMessage,
      getValidationStatus,
      hasError,
      pkceEnabled,
    };
  },
});
</script>
