export interface Role {
    id?: number;
    name: string;
    description: string;
    protected: boolean;
}