import { UrlType } from '@/core/types/UrlType';

export interface ClientRedirectUrl {
    id: number;
    clientId: number;
    urlType: UrlType;
    url: string;
    isPrimary: boolean;
}