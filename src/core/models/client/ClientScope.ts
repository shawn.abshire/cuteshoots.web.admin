export interface ClientScope {
    id?: number;
    clientId?: number;
    scope: string;
}