import { GrantType } from '@/core/types/GrantType';

export interface ClientGrantType {
    id?: number;
    clientId?: number;
    grantType?: GrantType;
}
