export interface ClientSecret {
    id: number;
    clientId: number;
    description: string;
    value: string;
    expiration?: Date;
    type: string;
    created: Date;
}