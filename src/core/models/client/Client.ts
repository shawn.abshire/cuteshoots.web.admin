import { ClientCorsOrigin } from './ClientCorsOrigin';
import { ClientGrantType } from './ClientGrantType';
import { ClientRedirectUrl } from './ClientRedirectUrl';
import { ClientScope } from './ClientScope';
import { ClientSecret } from './ClientSecret';

export interface Client {
    id: number;
    name: string;
    clientGuid: string;
    pkceEnabled: boolean;
    grantType: ClientGrantType;
    scopes?: ClientScope[];
    redirectUrls?: ClientRedirectUrl[];
    secrets?: ClientSecret[];
    corsOrigin?: ClientCorsOrigin[];
}
