export interface ClientCorsOrigin {
    id: number;
    clientId: number;
    origin: string;
}