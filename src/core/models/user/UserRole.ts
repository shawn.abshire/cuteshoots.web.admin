import { Role } from '@/core/models/role/Role';

export interface UserRole {
    id?: number;
    roleId?: number;
    userId?: number;
    role?: Role;
}