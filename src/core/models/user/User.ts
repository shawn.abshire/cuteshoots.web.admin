import { UserRole } from './UserRole';

export interface User {
    id: number;
    email: string;
    firstName: string;
    lastName: string;
    enabled: boolean;
    roles: UserRole[];
    working?: boolean;
}