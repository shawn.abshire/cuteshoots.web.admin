export enum GrantType {
  Implicit = 'IMPLICIT',
  ImplicitAndClientCredentials = 'IMPLICIT_AND_CLIENT_CREDENTIALS',
  Code = 'CODE',
  CodeAndClientCredentials = 'CODE_AND_CLIENT_CREDENTIALS',
  Hybrid = 'HYBRID',
  HybridAndClientCredentials = 'HYBRID_AND_CLIENT_CREDENTIALS',
  ClientCredentials = 'CLIENT_CREDENTIALS',
  ResourceOwnerPassword = 'RESOURCE_OWNER_PASSWORD',
  ResourceOwnerPasswordAndClientCredentials = 'RESOURCE_OWNER_PASSWORD_AND_CLIENT_CREDENTIALS',
  DeviceFlow = 'DEVICE_FLOW'
}
