// TODO(Shawn): Remove when provided but the lull ui.
export interface SelectOption {
    value: string | number;
    label: string | number;
}