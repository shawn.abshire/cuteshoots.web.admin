import { PageInfo } from './PageInfo';

export interface Connection<T> {
    pageInfo: PageInfo;
    items?: T[];
    edges?: T[];
    totalCount: number;
}