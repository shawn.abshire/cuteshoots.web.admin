export enum UrlType {
    LoginRedirect = 'LOGIN_REDIRECT',
    LogoutRedirect = 'LOGOUT_REDIRECT'
}