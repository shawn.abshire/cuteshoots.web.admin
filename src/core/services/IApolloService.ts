import { ApolloQueryResult, OperationVariables, QueryBaseOptions } from 'apollo-client';
import { FetchResult } from 'apollo-link';

// eslint-disable-next-line @typescript-eslint/interface-name-prefix
export interface IApolloService {
    query<T>({ query, variables }: QueryBaseOptions): Promise<ApolloQueryResult<T>>;
    mutate<T>({ mutation, variables }: OperationVariables): Promise<FetchResult<T>>;
}