import { Profile } from 'oidc-client';

export interface IAuthService {
    login(): Promise<void>;
    logout(): Promise<void>;

    getIdentity(): Promise<Profile | undefined>;

    getAuthToken(): Promise<string>;
    hasRoleClaim(claim: string): Promise<boolean>;
    renewToken(): Promise<string>;
}
