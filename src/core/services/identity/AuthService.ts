/* eslint-disable @typescript-eslint/camelcase */
import { Profile, UserManager } from 'oidc-client';
import { IAuthService } from './IAuthService';

const oidcConfig: Oidc.UserManagerSettings = {
  authority: import.meta.env.VITE_APP_AUTHORITY as string,
  client_id: import.meta.env.VITE_APP_CLIENTID as string,
  redirect_uri: `${location.origin}/callback.html`,
  response_type: 'code',
  scope: 'openid profile',
  post_logout_redirect_uri: `${location.origin}`,
  silent_redirect_uri: `${location.origin}/silent-refresh.html`
};

export class AuthService implements IAuthService {
  private readonly userManager = new UserManager(oidcConfig);
  private tokenRenewalPromise: Promise<string> | null = null;

  public async login(): Promise<void> {
    await this.userManager.signinRedirect();
  }

  public async logout(): Promise<void> {
    await this.userManager.signoutRedirect();
  }

  public async getIdentity(): Promise<Profile | undefined> {
    const user = await this.userManager.getUser();

    return user?.profile;
  }

  public async getAuthToken(): Promise<string> {
    const user = await this.userManager.getUser();
    if (user) {
      return user.access_token;
    }

    return '';
  }

  public async hasRoleClaim(claim: string): Promise<boolean> {
    const user = await this.getIdentity();

    return user?.role.includes(claim) || false;
  }

  public async renewToken(): Promise<string> {
    this.tokenRenewalPromise = this.tokenRenewalPromise || this._renewToken();

    return this.tokenRenewalPromise;
  }

  private async _renewToken(): Promise<string> {
    try {
      await this.userManager.signinSilent();

      return await this.getAuthToken();
    } catch (err) {
      switch (err.message) {
        case 'login_required':
          await this.userManager.signinRedirect();
          throw new Error('Redirecting to login');
        default:
          throw err;
      }
    } finally {
      this.tokenRenewalPromise = null;
    }
  }
}