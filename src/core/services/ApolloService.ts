import { apolloClient as apollo } from '@/core/ApolloClient';
import { ApolloQueryResult, FetchPolicy, OperationVariables, QueryBaseOptions } from 'apollo-client';
import { FetchResult } from 'apollo-link';
import { IApolloService } from './IApolloService';

export class ApolloService implements IApolloService {
  private fetchPolicy: FetchPolicy;

  constructor(fetchPolicy: FetchPolicy = 'cache-first') {
    this.fetchPolicy = fetchPolicy;
  }

  public async query<T>({ query, variables }: QueryBaseOptions): Promise<ApolloQueryResult<T>> {
    const response = await apollo.query({
      fetchPolicy: this.fetchPolicy,
      query: query,
      variables: variables,
      errorPolicy: 'all'
    });

    return response;
  }

  public async mutate<T>({ mutation, variables }: OperationVariables): Promise<FetchResult<T>> {
    const response = await apollo.mutate({
      mutation: mutation,
      variables: variables,
      errorPolicy: 'all'
    });

    if (response.errors) {
      const messages = response.errors.map(x => x.message).join(',');

      throw Error(messages);
    }

    return response;
  }
}