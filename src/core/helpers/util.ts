interface SelectOption {
    label: string | number;
    value: string | number;
}

export const isUndefined = (value: unknown): value is undefined => typeof value === "undefined" || value === null;

export const capitalizeFirstLetter = (value: string, locale = navigator.language) => value.charAt(0).toLocaleUpperCase(locale) + value.slice(1);

export const enumToSelectOptions = (value: Record<string, string | number>): SelectOption[] => {
    // const results = Object.keys(value).filter(e => !isNaN(+e)).map(o => { return { value: +o, label: value[o] } });
    const results = Object.keys(value).map(o => { return { value: +o, label: value[o] } });

    return results;
}

export const uuidv4 = (): string => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);

        return v.toString(16);
    });
}
