// src/composables/useUserRepositories.js

import { capitalizeFirstLetter } from '@/core/helpers/util';
import useVuelidate, { ErrorObject } from '@vuelidate/core';
import { Ref } from 'vue';

// This matches the vuelidator interface, which is NOT exported...
interface GlobalConfig {
  $registerAs?: string;
  $scope?: string | number | symbol;
  $stopPropagation?: boolean;
  $autoDirty?: boolean;
  $lazy?: boolean;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default function useValidation(rules: any, state: object, config: GlobalConfig = {}) {
  const validation = useVuelidate(rules, state, config);

  function getValidator(property: string): ErrorObject | undefined {
    const validator = validation.value.$errors.find(
      (x) => x.$property === property
    );

    return validator;
  }

  function getValidationMessage(property: string): string | Ref<string> | undefined {
    const validator = getValidator(property);
    const message = validator?.$message.toString();
    const results = message?.replace('Value', capitalizeFirstLetter((validator as ErrorObject).$property));
    if (results) {
      return results;
    }

    return undefined;
  }

  function hasError(property: string): boolean {
    const validator = getValidator(property);
    const results = Boolean(validator?.$message);

    return results;
  }

  function getValidationStatus(property: string): string {
    const validator = getValidator(property);

    return validator?.$message !== undefined && validator?.$message !== '' ? 'danger' : 'default';
  }

  return { validation, getValidationMessage, getValidationStatus, hasError }
}