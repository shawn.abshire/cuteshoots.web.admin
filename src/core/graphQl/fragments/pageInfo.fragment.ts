import gql from 'graphql-tag';

export const pageInfoFragment = (type: string) => gql`
  fragment pageInfo${type} on ${type} {
    pageInfo {
      endCursor
      hasNextPage
      hasPreviousPage
      startCursor
    }
  }
`;
