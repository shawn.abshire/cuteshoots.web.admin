
import gql from 'graphql-tag';

export const getClientQuery = gql`
  query($clientId: String!) {
    client(clientId: $clientId) {
      id
      clientGuid
      name
      pkceEnabled
      grantType {
        id
        grantType
      }
      scopes {
        id
        scope
      }
   }
  }
`;

