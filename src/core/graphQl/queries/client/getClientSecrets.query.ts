import gql from 'graphql-tag';

export const getClientSecrets = gql`
  query($clientId: String!) {
    getClientSecrets(clientId: $clientId) {
      clientId
      created
      description
      expiration
      id
      type
      value
    }
  }
`
