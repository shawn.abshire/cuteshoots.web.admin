import { pageInfoFragment } from '@/core/graphQl/fragments/pageInfo.fragment';
import gql from 'graphql-tag';

export const getClientsQuery = gql`
  query($before: String, $after: String, $filter: DatatableFilter) {
    clients(before: $before, after: $after, filter: $filter) {
      items {
        name
        clientGuid
        grantType {
          grantType
        }
      }
      ...pageInfoClientConnection
      totalCount
    }
  }
  ${pageInfoFragment('ClientConnection')}
`;

