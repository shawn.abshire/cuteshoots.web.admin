import gql from 'graphql-tag';

export const getClientRedirectUrls = gql`
  query($clientId: String!) {
    getClientRedirectUrls(clientId: $clientId) {
      id
      url
      urlType
      isPrimary
    }
  }
`
