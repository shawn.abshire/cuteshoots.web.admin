export { getClientQuery } from './getClient.query';
export { getClientCorsOrigins } from './getClientCorsOrigins.query';
export { getClientRedirectUrls } from './getClientRedirectUrls.query';
export { getClientsQuery } from './getClients.query';
export { getClientSecrets } from './getClientSecrets.query';

