import gql from 'graphql-tag';

export const getClientCorsOrigins = gql`
  query($clientId: String!) {
    getClientCorsOrigins(clientId: $clientId) {
      id
      clientId
      origin
    }
  }
`
