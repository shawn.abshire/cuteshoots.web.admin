import gql from 'graphql-tag';

export const getRoleQuery = gql`
  query($id: String!) {
    role(id: $id) {
      id
      name
      description
      protected
    }
  }
`;