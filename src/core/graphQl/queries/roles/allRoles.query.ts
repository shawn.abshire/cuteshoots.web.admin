import gql from 'graphql-tag';

export const allRolesQuery = gql`
  query {
    allRoles {
      id
      name
      description
      protected
    }
  }
`;