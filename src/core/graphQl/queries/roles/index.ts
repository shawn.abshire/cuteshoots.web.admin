export { allRolesQuery } from './allRoles.query';
export { getRoleQuery } from './getRole.query';
export { getRolesQuery } from './getRoles.query';

