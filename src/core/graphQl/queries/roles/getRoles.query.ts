import { pageInfoFragment } from '@/core/graphQl/fragments/pageInfo.fragment';
import gql from 'graphql-tag';

export const getRolesQuery = gql`
  query($before: String, $after: String, $filter: DatatableFilter) {
    roles(before: $before, after: $after, filter: $filter) {
      items {
        id
        name
        description
        protected
        totalUsers
      }
      ...pageInfoRoleConnection
      totalCount
    }
  }
  ${pageInfoFragment('RoleConnection')}
`;

