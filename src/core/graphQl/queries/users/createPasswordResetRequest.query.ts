import gql from 'graphql-tag';

export const createPasswordResetRequest = gql`
  query($username: String!) {
    createPasswordResetRequest(username: $username)
  }
`;