import gql from 'graphql-tag';

export const getUserQuery = gql`
 query($id: String!) {
  user(id: $id) {
    id
    email
    firstName
    lastName
    enabled
    roles {
      role {
        id
        name
        description
        protected
      }
    }
  }
}
`;

