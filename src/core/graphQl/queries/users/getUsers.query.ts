import { pageInfoFragment } from '@/core/graphQl/fragments/pageInfo.fragment';
import gql from 'graphql-tag';

export const getUsersQuery = gql`
  query($before: String, $after: String, $filter: DatatableFilter) {
    users(before: $before, after: $after, filter: $filter) {
      items {
        id
        email
        firstName
        lastName
        enabled
        roles {
          role {
            name
          }
        }
      }
      ...pageInfoUserConnection
      totalCount
    }
  }
  ${pageInfoFragment('UserConnection')}
`;

