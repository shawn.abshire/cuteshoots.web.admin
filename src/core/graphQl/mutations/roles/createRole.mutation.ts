import gql from 'graphql-tag';

export const createRoleMutation = gql`
  mutation($role: RoleInput!) {
    createRole(role: $role) {
      id
      name
      description
    }
  }
`;
