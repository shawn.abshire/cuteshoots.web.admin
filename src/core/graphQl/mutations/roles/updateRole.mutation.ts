import gql from 'graphql-tag';

export const updateRoleMutation = gql`
  mutation($role: RoleInput!) {
    updateRole(role: $role) {
      id
      name
      description
    }
  }
`;
