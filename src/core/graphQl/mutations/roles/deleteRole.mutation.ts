
import gql from 'graphql-tag';

export const deleteRoleMutation = gql`
  mutation($id: Int!) {
    deleteRole(id: $id)
  }
`;

