import gql from 'graphql-tag';

const userInfoFragment = gql`
  fragment user on User {
    id
    email
    firstName
    lastName
    enabled
  }
`;

export const deactivateUserMutation = gql`
  mutation($id: Int!) {
    deactivateUser(id: $id) {
      ...user
    }
  }
  ${userInfoFragment}
`;

export const activateUserMutation = gql`
  mutation($id: Int!) {
    activateUser(id: $id) {
      ...user
    }
  }
  ${userInfoFragment}
`;

