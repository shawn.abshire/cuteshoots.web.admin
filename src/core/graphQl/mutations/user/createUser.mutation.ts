import gql from 'graphql-tag';

export const createUserMutation = gql`
  mutation($user: UserInput!, $roles: [RoleInput]) {
    createUser(user: $user, roles: $roles) {
      id
      email
      firstName
      lastName
      enabled
    }
  }
`;
