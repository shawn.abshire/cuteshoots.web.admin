import gql from 'graphql-tag';

export const updateUserMutation = gql`
  mutation($user: UserInput!, $roles: [RoleInput]) {
    updateUser(user: $user, roles: $roles) {
      id
    }
  }
`;
