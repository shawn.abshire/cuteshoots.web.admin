import gql from 'graphql-tag';

export const addClientMutation = gql`
  mutation($client: ClientInput!){
    addClient(client: $client){
      id
      clientGuid
      name
      pkceEnabled
      grantType {
        id
        clientId
        grantType
      }
    }
  }
`