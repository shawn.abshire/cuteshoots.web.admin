export { addClientMutation } from './addClient.mutation';
export { addCorsOriginMutation } from './addCorsOrigin.mutation';
export { addRedirectUrlMutation } from './addRedirectUrl.mutation';
export { addSecretMutation } from './addSecret.mutation';
export { deleteClientMutation } from './deleteClient.mutation';
export { deleteCorsOriginMutation } from './deleteCorsOrigin.mutation';
export { deleteRedirectUrlMutation } from './deleteRedirectUrl.mutation';
export { deleteSecretMutation } from './deleteSecret.mutation';
export { updateClientMutation } from './updateClient.mutation';
export { updateClientRedirectUrlMutation } from './updateClientRedirectUrl.mutation';
export { updateCorsOriginMutation } from './updateCorsOrigin.mutation';
export { updateSecretMutation } from './updateSecret.mutation';

