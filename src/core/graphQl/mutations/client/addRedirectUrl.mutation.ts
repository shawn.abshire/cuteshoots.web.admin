import gql from 'graphql-tag';

export const addRedirectUrlMutation = gql`
  mutation ($clientId: String!, $redirectUrl: ClientRedirectUrlInput!) {
    addRedirectUrl(clientId: $clientId, redirectUrl: $redirectUrl) {
      id
      url
      urlType
      isPrimary
    }
  }
`
