import gql from 'graphql-tag';

export const deleteRedirectUrlMutation = gql`
  mutation($id: Int!) {
    deleteRedirectUrl(id: $id)
  }
`;

