import gql from 'graphql-tag';

export const addCorsOriginMutation = gql`
  mutation($clientId: String!, $origin: ClientCorsOriginInput!) {
    addCorsOrigin(clientId: $clientId, origin: $origin) {
      id
      clientId
      origin
    }
  }
`

