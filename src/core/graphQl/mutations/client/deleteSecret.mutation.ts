import gql from 'graphql-tag';

export const deleteSecretMutation = gql`
  mutation($id: Int!) {
    deleteSecret(id: $id)
  }
`;

