import gql from 'graphql-tag';

export const deleteClientMutation = gql`
  mutation($clientId: String!) {
    deleteClient(clientId: $clientId)
  }
`;

