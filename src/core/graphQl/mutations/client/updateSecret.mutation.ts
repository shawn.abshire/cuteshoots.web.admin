import gql from 'graphql-tag';

export const updateSecretMutation = gql`
  mutation($secret: ClientSecretInput!) {
    updateSecret(secret: $secret) {
      id
      created
      description
      expiration
      type
      value
    }
  }
`