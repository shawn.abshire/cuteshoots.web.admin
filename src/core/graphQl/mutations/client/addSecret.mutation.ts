import gql from 'graphql-tag';

export const addSecretMutation = gql`
  mutation($clientId: String!, $secret: ClientSecretInput!) {
    addSecret(clientId: $clientId, secret: $secret) {
      clientId
      created
      description
      expiration
      id
      type
      value
    }
  }
`

