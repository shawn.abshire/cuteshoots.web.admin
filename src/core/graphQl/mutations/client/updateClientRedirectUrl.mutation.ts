import gql from 'graphql-tag';

export const updateClientRedirectUrlMutation = gql`
  mutation($redirectUrl: ClientRedirectUrlInput!) {
    updateRedirectUrl(redirectUrl: $redirectUrl) {
      id
      isPrimary
      url
      urlType
    }
  }
`