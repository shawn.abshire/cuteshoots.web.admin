import gql from 'graphql-tag';

export const deleteCorsOriginMutation = gql`
  mutation($id: Int!) {
    deleteCorsOrigin(id: $id)
  }
`;

