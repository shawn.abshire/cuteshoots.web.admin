import gql from 'graphql-tag';

export const updateClientMutation = gql`
  mutation($client: ClientInput!) {
    updateClient(client: $client) {
      id
      clientGuid
      scopes {
        id
        scope
      }
    }
  }
`