import gql from 'graphql-tag';

export const updateCorsOriginMutation = gql`
  mutation($origin: ClientCorsOriginInput!){
    updateCorsOrigin(origin: $origin){
      id
      clientId
      origin
    }
  }
`