import { InMemoryCache } from "apollo-cache-inmemory";
import ApolloClient from "apollo-client";
import { ApolloLink } from 'apollo-link';
import { setContext } from 'apollo-link-context';
import { HttpLink } from 'apollo-link-http';
import { AuthService } from './services/identity/AuthService';

const httpLink = new HttpLink({
  uri: import.meta.env.VITE_APP_GRAPHQL_URL as string,
});


const authLink = setContext(async (_, { headers }) => {
  const authService = new AuthService();
  const accessToken = await authService.getAuthToken();

  return {
    headers: {
      ...headers,
      authorization: accessToken ? `Bearer ${accessToken}` : '',
    },
  };
});

const cleanTypeName = new ApolloLink((operation, forward) => {
  if (operation.variables) {
    const omitTypename = (key: string, value: unknown) => (key === '__typename' ? undefined : value);
    operation.variables = JSON.parse(JSON.stringify(operation.variables), omitTypename);
  }

  return forward(operation).map((data) => {
    return data;
  });
});

const link = ApolloLink.from([
  authLink,
  cleanTypeName,
  httpLink,
]);

export const apolloClient = new ApolloClient({
  link: link,
  cache: new InMemoryCache({
    addTypename: true
  }),
});
