import { IToast } from 'lull-ui';
import { Store } from 'vuex';

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $store: Store<State>;
    $toast: IToast;
  }
}