import { deleteClientMutation } from '@/core/graphQl/mutations/client';
import { getClientsQuery } from '@/core/graphQl/queries';
import { isUndefined } from '@/core/helpers/util';
import { Client } from '@/core/models/client/Client';
import { Connection } from '@/core/types/Connection';
import { Module } from 'vuex';
import { ApolloAction } from '../ApolloModule';
import { RootState } from '../rootState';

export interface ClientsState {
  loading: boolean;
  clients: Connection<Client> | null;
}

export enum ClientsAction {
  GetClients = 'GetClients',
  DeleteClient = 'DeleteClient',
}

export enum ClientsMutation {
  SetLoading = 'SetLoadingState',
  SetClients = 'SetClients',
  RemoveClient = 'RemoveClient',
}

export const ClientsModule: Module<ClientsState, RootState> = {
  namespaced: true,
  state: {
    loading: true,
    clients: null,
  },
  getters: {
    clientsLoadingState: (state) => state.loading,
    clients: (state) => state.clients,
  },
  mutations: {
    [ClientsMutation.SetLoading]: (state, value: boolean) => state.loading = value,
    [ClientsMutation.SetClients]: (state, value: Connection<Client>) => state.clients = value,
    [ClientsMutation.RemoveClient]: (state, clientId: string) => {
      const index = state.clients?.items?.findIndex((x: Client) => x.clientGuid === clientId);
      if (!isUndefined(index) && index > -1 && state.clients) {
        const clients = state.clients?.items?.filter((x: Client) => x.clientGuid !== clientId);
        state.clients.items = clients;
        state.clients.totalCount--;
      }
    }
  },
  actions: {
    [ClientsAction.GetClients]: async ({ commit, dispatch }, { before, after, filter }) => {
      commit(ClientsMutation.SetLoading, true);
      const response = await dispatch(ApolloAction.Query, {
        query: getClientsQuery,
        variables: {
          before: before,
          after: after,
          filter: filter
        },
        fetchPolicy: 'no-cache'
      }, { root: true });
      commit(ClientsMutation.SetClients, response.data.clients);
      commit(ClientsMutation.SetLoading, false);
    },
    [ClientsAction.DeleteClient]: async ({ commit, dispatch }, clientId: string) => {
      commit(ClientsMutation.SetLoading, false);
      const response = await dispatch(ApolloAction.Mutate, {
        mutation: deleteClientMutation,
        variables: {
          clientId: clientId
        }
      }, { root: true });

      if (response.data.deleteClient === true) {
        commit(ClientsMutation.RemoveClient, clientId);
      }
      commit(ClientsMutation.SetLoading, false);
    }
  },
}