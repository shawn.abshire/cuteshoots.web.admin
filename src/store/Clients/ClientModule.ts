import {
  addClientMutation,
  addCorsOriginMutation,
  addRedirectUrlMutation,
  addSecretMutation,
  deleteCorsOriginMutation,
  deleteRedirectUrlMutation,
  deleteSecretMutation,
  updateClientMutation,
  updateClientRedirectUrlMutation,
  updateCorsOriginMutation,
  updateSecretMutation
} from '@/core/graphQl/mutations/client';
import { getClientCorsOrigins, getClientQuery, getClientRedirectUrls, getClientSecrets } from '@/core/graphQl/queries/client';
import { Client } from '@/core/models/client/Client';
import { ClientCorsOrigin } from '@/core/models/client/ClientCorsOrigin';
import { ClientRedirectUrl } from '@/core/models/client/ClientRedirectUrl';
import { ClientSecret } from '@/core/models/client/ClientSecret';
import { Module } from 'vuex';
import { ApolloAction } from '../ApolloModule';
import { RootState } from '../rootState';

export interface ClientState {
  clientLoading: boolean;
  redirectUrlsLoading: boolean;
  secretsLoading: boolean;
  corsOriginLoading: boolean;
}

export enum ClientAction {
  GetClient = 'GetClient',
  GetRedirectUrls = 'GetRedirectUrls',
  GetSecrets = 'GetSecrets',
  GetCorsOrigins = 'GetCorsOrigins',
  AddClient = 'AddClient',
  UpdateClient = 'UpdateClient',
  AddRedirectUrl = 'AddRedirectUrl',
  UpdateRedirectUrl = 'UpdateRedirectUrl',
  DeleteRedirectUrl = 'DeleteRedirectUrl',
  AddSecret = 'AddSecret',
  UpdateSecret = 'UpdateSecret',
  DeleteSecret = 'DeleteSecret',
  AddCorsOrigin = 'AddCorsOrigin',
  UpdateCorsOrigin = 'UpdateCorsOrigin',
  DeleteCorsOrigin = 'DelteCorsOrigin',
}

export enum ClientMutation {
  SetClientLoading = 'SetClientLoading',
  SetRedirectUrlsLoading = 'SetRedirectUrlsLoading',
  SetSecretsLoading = 'SetSecretsLoading',
  SetCorsOriginLoading = 'SetCorsOriginLoading'
}

export const ClientModule: Module<ClientState, RootState> = {
  namespaced: true,
  state: {
    clientLoading: true,
    redirectUrlsLoading: true,
    secretsLoading: true,
    corsOriginLoading: true,
  },
  getters: {
    clientLoadingState: (state) => state.clientLoading,
    redirectUrlsLoadingState: (state) => state.redirectUrlsLoading,
    secretsLoadingState: (state) => state.secretsLoading,
    corsOriginLoadingState: (state) => state.corsOriginLoading,
  },
  mutations: {
    [ClientMutation.SetClientLoading]: (state, value: boolean) => state.clientLoading = value,
    [ClientMutation.SetRedirectUrlsLoading]: (state, value: boolean) => state.redirectUrlsLoading = value,
    [ClientMutation.SetSecretsLoading]: (state, value: boolean) => state.secretsLoading = value,
    [ClientMutation.SetCorsOriginLoading]: (state, value: boolean) => state.corsOriginLoading = value,
  },
  actions: {
    [ClientAction.GetClient]: async ({ commit, dispatch }, clientId: string) => {
      commit(ClientMutation.SetClientLoading, true)
      const response = await dispatch(ApolloAction.Query, {
        query: getClientQuery,
        variables: { clientId: clientId },
        fetchPolicy: 'no-cache'
      }, { root: true });
      commit(ClientMutation.SetClientLoading, false)

      return response.data.client;
    },
    [ClientAction.AddClient]: async ({ commit, dispatch }, client: Client) => {
      commit(ClientMutation.SetClientLoading, true)
      const response = await dispatch(ApolloAction.Mutate, {
        mutation: addClientMutation, variables: { client: client }
      }, { root: true });
      commit(ClientMutation.SetClientLoading, false)

      return response.data.addClient.clientGuid;
    },
    [ClientAction.UpdateClient]: async ({ commit, dispatch }, client: Partial<Client>) => {
      commit(ClientMutation.SetClientLoading, true)
      const response = await dispatch(ApolloAction.Mutate, {
        mutation: updateClientMutation, variables: {
          client: client
        }
      }, { root: true });
      commit(ClientMutation.SetClientLoading, false)

      return response.data.updateClient;
    },
    [ClientAction.GetRedirectUrls]: async ({ commit, dispatch }, clientId: string) => {
      commit(ClientMutation.SetRedirectUrlsLoading, true);
      const response = await dispatch(ApolloAction.Query, {
        query: getClientRedirectUrls,
        variables: {
          clientId: clientId
        },
        fetchPolicy: 'no-cache'
      }, { root: true });
      commit(ClientMutation.SetRedirectUrlsLoading, false);

      return response.data.getClientRedirectUrls;
    },
    [ClientAction.AddRedirectUrl]: async ({ commit, dispatch }, { clientId, redirectUrl }: { clientId: string; redirectUrl: ClientRedirectUrl }) => {
      commit(ClientMutation.SetRedirectUrlsLoading, true)
      const response = await dispatch(ApolloAction.Mutate, {
        mutation: addRedirectUrlMutation,
        variables: {
          clientId: clientId,
          redirectUrl: redirectUrl
        }
      }, { root: true });
      commit(ClientMutation.SetRedirectUrlsLoading, false)

      return response.data.addRedirectUrl;
    },
    [ClientAction.UpdateRedirectUrl]: async ({ commit, dispatch }, { redirectUrl }: { redirectUrl: ClientRedirectUrl }) => {
      commit(ClientMutation.SetRedirectUrlsLoading, true)
      const response = await dispatch(ApolloAction.Mutate, {
        mutation: updateClientRedirectUrlMutation,
        variables: {
          redirectUrl: redirectUrl
        }
      }, { root: true });
      commit(ClientMutation.SetRedirectUrlsLoading, false)

      return response.data.updateRedirectUrl;
    },
    [ClientAction.DeleteRedirectUrl]: async ({ commit, dispatch }, id: number) => {
      commit(ClientMutation.SetRedirectUrlsLoading, true)
      const response = await dispatch(ApolloAction.Mutate, {
        mutation: deleteRedirectUrlMutation,
        variables: {
          id: id
        }
      }, { root: true });
      commit(ClientMutation.SetRedirectUrlsLoading, false)

      return response.data.deleteRedirectUrl;
    },
    [ClientAction.GetSecrets]: async ({ commit, dispatch }, clientId: string) => {
      commit(ClientMutation.SetSecretsLoading, true)
      const response = await dispatch(ApolloAction.Query, {
        query: getClientSecrets,
        variables: {
          clientId: clientId
        },
        fetchPolicy: 'no-cache'
      }, { root: true });
      commit(ClientMutation.SetSecretsLoading, false)

      return response.data.getClientSecrets;
    },
    [ClientAction.AddSecret]: async ({ commit, dispatch }, { clientId, secret }: { clientId: string; secret: Partial<ClientSecret> }) => {
      commit(ClientMutation.SetSecretsLoading, true)
      const response = await dispatch(ApolloAction.Mutate, {
        mutation: addSecretMutation,
        variables: {
          clientId: clientId,
          secret: secret
        }
      }, { root: true });
      commit(ClientMutation.SetSecretsLoading, false)

      return response.data.addSecret;
    },
    [ClientAction.UpdateSecret]: async ({ commit, dispatch }, { secret }: { secret: ClientSecret }) => {
      commit(ClientMutation.SetSecretsLoading, true)
      const response = await dispatch(ApolloAction.Mutate, {
        mutation: updateSecretMutation,
        variables: {
          secret: secret
        }
      }, { root: true });
      commit(ClientMutation.SetSecretsLoading, false)

      return response.data.updateSecret;
    },
    [ClientAction.DeleteSecret]: async ({ commit, dispatch }, id: number) => {
      commit(ClientMutation.SetSecretsLoading, true)
      const response = await dispatch(ApolloAction.Mutate, {
        mutation: deleteSecretMutation,
        variables: {
          id: id
        }
      }, { root: true });
      commit(ClientMutation.SetSecretsLoading, false)

      return response.data.deleteSecret;
    },
    [ClientAction.GetCorsOrigins]: async ({ commit, dispatch }, clientId: string) => {
      commit(ClientMutation.SetCorsOriginLoading, true)
      const response = await dispatch(ApolloAction.Query, {
        query: getClientCorsOrigins,
        variables: {
          clientId: clientId
        },
        fetchPolicy: 'no-cache'
      }, { root: true });
      commit(ClientMutation.SetCorsOriginLoading, false)

      return response.data.getClientCorsOrigins;
    },
    [ClientAction.AddCorsOrigin]: async ({ commit, dispatch }, { clientId, origin }: { clientId: string; origin: Partial<ClientCorsOrigin> }) => {
      commit(ClientMutation.SetCorsOriginLoading, true)
      const response = await dispatch(ApolloAction.Mutate, {
        mutation: addCorsOriginMutation,
        variables: {
          clientId: clientId,
          origin: origin
        }
      }, { root: true });
      commit(ClientMutation.SetCorsOriginLoading, false)

      return response.data.addCorsOrigin;
    },
    [ClientAction.UpdateCorsOrigin]: async ({ commit, dispatch }, { origin }: { origin: ClientCorsOrigin }) => {
      commit(ClientMutation.SetCorsOriginLoading, true)
      const response = await dispatch(ApolloAction.Mutate, {
        mutation: updateCorsOriginMutation,
        variables: {
          origin: origin
        }
      }, { root: true });
      commit(ClientMutation.SetCorsOriginLoading, false)

      return response.data.updateCorsOrigin;
    },
    [ClientAction.DeleteCorsOrigin]: async ({ commit, dispatch }, id: number) => {
      commit(ClientMutation.SetCorsOriginLoading, true)
      const response = await dispatch(ApolloAction.Mutate, {
        mutation: deleteCorsOriginMutation,
        variables: {
          id: id
        }
      }, { root: true });
      commit(ClientMutation.SetCorsOriginLoading, false)

      return response.data.deleteCorsOrigin;
    }
  }
}