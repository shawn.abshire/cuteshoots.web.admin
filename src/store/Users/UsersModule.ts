import { activateUserMutation, deactivateUserMutation } from '@/core/graphQl/mutations/user';
import { createPasswordResetRequest, getUsersQuery } from '@/core/graphQl/queries/users';
import { User } from '@/core/models/user';
import { Connection } from '@/core/types/Connection';
import { DatatableFilterData } from 'lull-ui';
import { Module } from 'vuex';
import { ApolloAction } from '../ApolloModule';
import { RootState } from '../rootState';

export interface UsersState {
  loading: boolean;
  users: Connection<User> | null;
}

export enum UsersAction {
  GetUsers = 'GetUsers',
  ToggleUser = 'ToggleUser',
  PasswordReset = 'PasswordResetRequest'
}

export enum UsersMutation {
  SetLoading = 'SetLoading',
  SetUsers = 'SetUsers',
  SetUser = 'SetUser',
  SetUserState = 'SetUserState'
}

export const UsersModule: Module<UsersState, RootState> = {
  namespaced: true,
  state: {
    loading: true,
    users: null
  },
  getters: {
    usersLoading: (state) => state.loading,
    users: (state) => state.users,
    userState: (state, id: number) => {
      const user = state.users?.items?.find(x => x.id === id);
      if (user) {
        return user.working;
      }

      return false;
    }
  },
  mutations: {
    [UsersMutation.SetLoading]: (state, value: boolean) => state.loading = value,
    [UsersMutation.SetUsers]: (state, users: Connection<User>) => state.users = users,
    [UsersMutation.SetUserState]: (state, { id, value }: { id: number; value: boolean }) => {
      const user = state.users?.items?.find(x => x.id === id);
      if (!user) {
        return;
      }

      user.working = value;
    },
    [UsersMutation.SetUser]: (state, user: User) => {
      const item = state.users?.items?.find(x => x.id === user.id);
      if (!item) {
        return;
      }

      Object.assign(item, { ...user, working: false });
    }
  },
  actions: {
    [UsersAction.GetUsers]: async ({ commit, dispatch }, { before, after, filter }: { before: string; after: string; filter: DatatableFilterData }) => {
      commit(UsersMutation.SetLoading, true);
      const response = await dispatch(ApolloAction.Query, {
        query: getUsersQuery,
        variables: {
          before: before,
          after: after,
          filter: filter
        },
        fetchPolicy: 'no-cache'
      }, { root: true });
      commit(UsersMutation.SetUsers, response.data.users);
      commit(UsersMutation.SetLoading, false);
    },
    [UsersAction.ToggleUser]: async ({ commit, dispatch }, { id, enabled }: { id: number; enabled: boolean }) => {
      commit(UsersMutation.SetUserState, { id: id, value: true });
      const mutation = enabled ? deactivateUserMutation : activateUserMutation;
      const responseAction = enabled ? 'deactivateUser' : 'activateUser';
      const response = await dispatch(ApolloAction.Mutate, {
        mutation: mutation,
        variables: {
          id: id
        },
        fetchPolicy: 'no-cache'
      }, { root: true })
      const user = response.data[responseAction];
      commit(UsersMutation.SetUser, user);
      return true;
    },
    [UsersAction.PasswordReset]: async ({ dispatch }, username: string) => {
      await dispatch(ApolloAction.Query, {
        query: createPasswordResetRequest,
        variables: {
          username: username
        }
      }, { root: true })

      return true;
    }
  }
}