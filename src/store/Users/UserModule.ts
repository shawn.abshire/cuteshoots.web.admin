
import { createUserMutation, updateUserMutation } from '@/core/graphQl/mutations/user';
import { allRolesQuery } from '@/core/graphQl/queries/roles';
import { getUserQuery } from '@/core/graphQl/queries/users';
import { Role } from '@/core/models/role/Role';
import { User } from '@/core/models/user';
import { UserRole } from '@/core/models/user/UserRole';
import { Module } from 'vuex';
import { ApolloAction } from '../ApolloModule';
import { RootState } from '../rootState';

export interface UserState {
  loading: boolean;
  roles: Role[];
}

export enum UserAction {
  GetUser = 'GetUser',
  CreateUser = 'CreateUser',
  UpdateUser = 'UpdateUser',
  GetRoles = 'GetRoles',
}

export enum UserMutation {
  SetLoading = 'SetLoading',
  SetRoles = 'SetRoles'
}

export const UserModule: Module<UserState, RootState> = {
  namespaced: true,
  state: {
    loading: true,
    roles: []
  },
  getters: {
    loading: (state) => state.loading,
    roles: (state) => state.roles
  },
  mutations: {
    [UserMutation.SetLoading]: (state, value: boolean) => state.loading = value,
    [UserMutation.SetRoles]: (state, value: Role[]) => state.roles = value,
  },
  actions: {
    [UserAction.GetUser]: async ({ commit, dispatch }, id: string) => {
      commit(UserMutation.SetLoading, true);
      const response = await dispatch(ApolloAction.Query, {
        query: getUserQuery,
        variables: {
          id: id,
        },
        fetchPolicy: 'no-cache'
      }, { root: true });
      const user = response.data.user;
      user.roles = user.roles.map((x: UserRole) => x.role);
      commit(UserMutation.SetLoading, false);

      return response.data.user;
    },
    [UserAction.CreateUser]: async ({ commit, dispatch }, { user, roles }: { user: Partial<User>; roles: Role[] }) => {
      commit(UserMutation.SetLoading, true);
      delete user.roles;
      const response = await dispatch(ApolloAction.Mutate, {
        mutation: createUserMutation,
        variables: {
          user: user,
          roles: roles,
        }
      }, { root: true });
      commit(UserMutation.SetLoading, false);

      return response.data.createUser.id;
    },
    [UserAction.UpdateUser]: async ({ commit, dispatch }, { user, roles }: { user: Partial<User>; roles: Role[] }) => {
      commit(UserMutation.SetLoading, true);
      delete user.roles;
      const response = await dispatch(ApolloAction.Mutate, {
        mutation: updateUserMutation,
        variables: {
          user: user,
          roles: roles
        }
      }, { root: true });
      commit(UserMutation.SetLoading, false);

      return response.data.updateUser.id;
    },
    [UserAction.GetRoles]: async ({ commit, dispatch }) => {
      const response = await dispatch(ApolloAction.Query, {
        query: allRolesQuery,
      }, { root: true });

      if (response) {
        commit(UserMutation.SetRoles, response.data.allRoles);
      }
    }
  }
}