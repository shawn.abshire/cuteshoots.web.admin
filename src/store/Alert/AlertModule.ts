import { uuidv4 } from '@/core/helpers/util';
import { GraphQLError } from 'graphql';
import { Module } from 'vuex';
import { RootState } from '../rootState';

interface Alert {
  id: string;
  message: string;
}

export interface AlertState {
  alerts: Alert[];
}

export enum AlertAction {
}

export enum AlertMutation {
  SetAlert = 'SetAlert',
  RemoveAlert = 'RemoveAlert',
  ClearAlerts = 'ClearAlert'
}

export const AlertModule: Module<AlertState, RootState> = {
  state: {
    alerts: []
  },
  getters: {
    alerts: (state) => state.alerts,
  },
  mutations: {
    [AlertMutation.SetAlert]: (state, message: string | GraphQLError[]) => {
      let messages: string | GraphQLError[] = message;
      if (typeof message === 'object') {
        messages = message.map(x => x.message).join(',');
      }
      state.alerts.push({ id: uuidv4(), message: messages as string })
    },
    [AlertMutation.ClearAlerts]: (state) => state.alerts = [],
    [AlertMutation.RemoveAlert]: (state, id: string) => {
      const results = state.alerts.filter(x => x.id !== id);
      state.alerts = results;
    }
  },
  actions: {}
}