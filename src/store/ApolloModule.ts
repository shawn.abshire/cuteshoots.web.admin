import { ApolloService } from '@/core/services/ApolloService';
import { IApolloService } from '@/core/services/IApolloService';
import { FetchPolicy, OperationVariables } from 'apollo-client';
import { DocumentNode } from 'graphql';
import { Module } from 'vuex';
import { AlertMutation } from './Alert/AlertModule';
import { RootState } from './rootState';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ApolloState { }

export enum ApolloAction {
  Query = 'apollo/query',
  Mutate = 'apollo/mutate',
}

export enum ApolloMutation {
  SetLoading = 'apollo/setLoading'
}

// TODO(Shawn Abshire): Add proper error handling.
// TODO(Shawn Abshire): Make a more generic CRUD module to handle 95% of cases?
export const ApolloModule: Module<{}, RootState> = {
  actions: {
    [ApolloAction.Query]: async ({ commit }, { query, variables, fetchPolicy = 'cache-first' }: { query: DocumentNode; variables: Partial<OperationVariables>; fetchPolicy: FetchPolicy }) => {
      try {
        const apollo: IApolloService = new ApolloService(fetchPolicy);
        const response = await apollo.query({
          query: query,
          variables: variables
        });

        if (response.errors && response.errors.length > 0) {
          commit(AlertMutation.SetAlert, response.errors);

          return;
        }

        return response;
      } catch (e) {
        commit(AlertMutation.SetAlert, e.message);
      }
    },
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [ApolloAction.Mutate]: async ({ commit }, { mutation, variables, fetchPolicy = 'cache-first' }: { mutation: any; variables: any; fetchPolicy: FetchPolicy }) => {
      try {
        const apollo: IApolloService = new ApolloService(fetchPolicy);
        const response = await apollo.mutate({
          mutation: mutation,
          variables: variables
        });

        return response;
      } catch (e) {
        commit(AlertMutation.SetAlert, e.message);
      }
    }
  }
}