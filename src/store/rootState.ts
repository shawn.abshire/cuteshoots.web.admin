import { AlertState } from './Alert/AlertModule';
import { ApolloState } from './ApolloModule';
import { AuthState } from './Auth/AuthModule';

export interface RootState {
  apollo: ApolloState;
  authState: AuthState;
  alert: AlertState;
}
