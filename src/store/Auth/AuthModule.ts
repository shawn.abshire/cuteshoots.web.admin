import { isUndefined } from '@/core/helpers/util';
import { AuthService } from '@/core/services/identity/AuthService';
import { IAuthService } from '@/core/services/identity/IAuthService';
import { Profile } from 'oidc-client';
import { Module } from 'vuex';
import { RootState } from '../rootState';

export interface AuthState {
    loading: boolean;
    user: Profile | null;
}

export enum AuthAction {
    GetUser = '[App] GetUser'
}

export enum AuthMutation {
    SetLoading = '[Auth] SetLoadingState',
    SetUser = '[Auth] SetUser'
}

export const AuthModule: Module<AuthState, RootState> = {
    state: {
        loading: true,
        user: null
    },
    getters: {
        userLoadingState: (state) => state.loading,
        userAuthenticated: (state) => !isUndefined(state.user) && state.loading === false,
        user: (state) => state.user
    },
    mutations: {
        [AuthMutation.SetLoading]: (state, value: boolean) => state.loading = value,
        [AuthMutation.SetUser]: (state, value: Profile) => state.user = value
    },
    actions: {
        [AuthAction.GetUser]: async ({ commit }) => {
            commit(AuthMutation.SetLoading, true)
            const authService: IAuthService = new AuthService();
            let user = await authService.getIdentity();
            if (user === undefined) {
                await authService.login();
                user = await authService.getIdentity();
            }

            commit(AuthMutation.SetUser, user);
            commit(AuthMutation.SetLoading, false);
        }
    }
}
