import { deleteRoleMutation } from '@/core/graphQl/mutations/roles';
import { allRolesQuery, getRolesQuery } from '@/core/graphQl/queries/roles';
import { isUndefined } from '@/core/helpers/util';
import { Role } from '@/core/models/role/Role';
import { Connection } from '@/core/types/Connection';
import { DatatableFilterData } from 'lull-ui';
import { Module } from 'vuex';
import { ApolloAction } from '../ApolloModule';
import { RootState } from '../rootState';

export interface RolesState {
  loading: boolean;
  roles: Connection<Role> | null;
}

export enum RolesAction {
  GetRoles = 'GetRoles',
  DeleteRole = 'DeleteRole',
  AllRoles = 'AllRoles'
}

export enum RolesMutation {
  SetLoading = 'SetLoading',
  SetRoles = 'SetRoles',
  RemoveRole = 'RemoveRole'
}

export const RolesModule: Module<RolesState, RootState> = {
  namespaced: true,
  state: {
    loading: true,
    roles: null
  },
  getters: {
    loading: (state) => state.loading,
    roles: (state) => state.roles
  },
  mutations: {
    [RolesMutation.SetLoading]: (state, value: boolean) => state.loading = value,
    [RolesMutation.SetRoles]: (state, roles: Connection<Role>) => state.roles = roles,
    [RolesMutation.RemoveRole]: (state, id: number) => {
      const roles = state.roles?.items?.filter((x: Role) => x.id !== id);
      if (state.roles) {
        state.roles.items = roles;
        state.roles.totalCount--;
      }
    }
  },
  actions: {
    [RolesAction.GetRoles]: async ({ commit, dispatch }, { before, after, filter }: { before: string; after: string; filter: DatatableFilterData }) => {
      commit(RolesMutation.SetLoading, true);
      const response = await dispatch(ApolloAction.Query, {
        query: getRolesQuery,
        variables: {
          before: before,
          after: after,
          filter: filter
        },
        fetchPolicy: 'no-cache'
      }, { root: true });

      commit(RolesMutation.SetLoading, false);
      commit(RolesMutation.SetRoles, response.data.roles);
    },
    [RolesAction.DeleteRole]: async ({ commit, dispatch }, id: number) => {
      commit(RolesMutation.SetLoading, true);
      const response = await dispatch(ApolloAction.Mutate, {
        mutation: deleteRoleMutation,
        variables: {
          id: id
        },
        fetchPolicy: 'no-cache'
      }, { root: true });
      const results = isUndefined(response) ? false : true;
      if (results) {
        commit(RolesMutation.RemoveRole, id);
      }

      commit(RolesMutation.SetLoading, false);
      return results;
    },
    [RolesAction.AllRoles]: async ({ commit, dispatch }) => {
      commit(RolesMutation.SetLoading, true);
      const response = await dispatch(ApolloAction.Query, {
        mutation: allRolesQuery,
        fetchPolicy: 'no-cache'
      }, { root: true });

      commit(RolesMutation.SetLoading, false);
      return response.data.allRoles;
    },
  }
}