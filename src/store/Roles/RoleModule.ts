import { createRoleMutation, updateRoleMutation } from '@/core/graphQl/mutations/roles';
import { getRoleQuery } from '@/core/graphQl/queries/roles';
import { Role } from '@/core/models/role/Role';
import { Module } from 'vuex';
import { ApolloAction } from '../ApolloModule';
import { RootState } from '../rootState';

export interface RoleState {
  loading: boolean;
}

export enum RoleAction {
  GetRole = 'GetRole',
  CreateRole = 'CreateRole',
  UpdateRole = 'UpdateRole',
}

export enum RoleMutation {
  SetLoading = 'SetLoading',
}

export const RoleModule: Module<RoleState, RootState> = {
  namespaced: true,
  state: {
    loading: true,
  },
  getters: {
    loading: (state) => state.loading,
  },
  mutations: {
    [RoleMutation.SetLoading]: (state, value: boolean) => state.loading = value,
  },
  actions: {
    [RoleAction.GetRole]: async ({ commit, dispatch }, id: number) => {
      commit(RoleMutation.SetLoading, true);
      const response = await dispatch(ApolloAction.Query, {
        query: getRoleQuery,
        variables: {
          id: id
        },
        fetchPolicy: 'no-cache'
      }, { root: true });
      commit(RoleMutation.SetLoading, false);

      return response.data.role;
    },
    [RoleAction.CreateRole]: async ({ commit, dispatch }, role: Partial<Role>) => {
      commit(RoleMutation.SetLoading, true);
      await dispatch(ApolloAction.Mutate, {
        mutation: createRoleMutation,
        variables: {
          role: role
        }
      }, { root: true });
      commit(RoleMutation.SetLoading, false);
    },
    [RoleAction.UpdateRole]: async ({ commit, dispatch }, role: Role) => {
      commit(RoleMutation.SetLoading, true);
      await dispatch(ApolloAction.Mutate, {
        mutation: updateRoleMutation,
        variables: {
          role: role
        }
      }, { root: true });
      commit(RoleMutation.SetLoading, false);
    }
  }
}