import { InjectionKey } from 'vue';
import { createLogger, createStore, Store, useStore as baseUseStore } from 'vuex';
import { AlertModule } from './Alert/AlertModule';
import { ApolloModule } from './ApolloModule';
import { AuthModule } from './Auth/AuthModule';
import { RootState } from './rootState';

export const key: InjectionKey<Store<RootState>> = Symbol()

// TODO(Shawn Abshire): Lets get some types in the store ya?
export const store = createStore<RootState>({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    apollo: ApolloModule,
    auth: AuthModule,
    alert: AlertModule
  },
  plugins: process.env.NODE_ENV !== 'production'
    ? [createLogger()]
    : []
});

export function useStore() {
  return baseUseStore(key)
}

export default store;