// Polyfills were copied from the Mozilla Developer wiki.
// Each of the snippets used here were added to the wiki on or after
// August 20th, 2010 and are public domain per the licensing on the mozilla site:
// https://developer.mozilla.org/en-US/docs/MDN/About
// As such, they have been copied, and then minified to save space

// Included are polyfills for:

// Array.from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/from#Polyfill
// Array.prototype.find: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find#Polyfill
// String.prototype.startsWith: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/startsWith#Polyfill

// These represent the minimum polyfills required to get the
// OIDC code working

// prettier-ignore
Array.from||(Array.from=function(){var r=Object.prototype.toString,t=function(t){return"function"==typeof t||"[object Function]"===r.call(t)},n=Math.pow(2,53)-1,e=function(r){var t=function(r){var t=Number(r);return isNaN(t)?0:0!==t&&isFinite(t)?(t>0?1:-1)*Math.floor(Math.abs(t)):t}(r);return Math.min(Math.max(t,0),n)};return function(r){var n=Object(r);if(null==r)throw new TypeError("Array.from requires an array-like object - not null or undefined");var o,i=arguments.length>1?arguments[1]:void 0;if(void 0!==i){if(!t(i))throw new TypeError("Array.from: when provided, the second argument must be a function");arguments.length>2&&(o=arguments[2])}for(var a,f=e(n.length),u=t(this)?Object(new this(f)):new Array(f),c=0;c<f;)a=n[c],u[c]=i?void 0===o?i(a,c):i.call(o,a,c):a,c+=1;return u.length=f,u}}()),Array.prototype.find||Object.defineProperty(Array.prototype,"find",{value:function(r){if(null==this)throw TypeError('"this" is null or not defined');var t=Object(this),n=t.length>>>0;if("function"!=typeof r)throw TypeError("predicate must be a function");for(var e=arguments[1],o=0;o<n;){var i=t[o];if(r.call(e,i,o,t))return i;o++}},configurable:!0,writable:!0}),String.prototype.startsWith||(String.prototype.startsWith=function(r,t){var n=t>0?0|t:0;return this.substring(n,n+r.length)===r});
